<!--
	vim: tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab
	vim: textwidth=80 colorcolumn=80
  -->

# My \*NIX Rice

In order to force myself to clean up my Void Linux dotfiles, I began ricing
FreeBSD in a Hyper-V and dragging over everything from via USB, cleaning up what
needed to be cleaned-up, committing at logical points, and not shying away from
scrapping everything and starting over from scratch.  I have tried to break
things up as logically as possible and document as much as possible.

## Tracking the Dotfiles

I am using a git bare repo.  This makes more sense to me than using GNU Stow,
though it is a bit messier in its own ways.

GNU Stow takes a base directory, say `~/.local/src/dotfiles`.  In this single
directory is a copy of the desired structure you would like your home directory
to keep.  When running stow, it will create all the uncreated directories from
dotfiles in your home, and then it will create sym links of all regular files
files.  This means that the git repository located in dotfiles is very clean and
only has the files you want it to have.  This seems like the perfect method to
deal with dot files.  By contrast, the git bare repository method seems very
messy.

When you make an empty file and initialize it as a git repo, it will be
populated with a hidden directory, `.git`.  It is in this hidden directory that
git actually stores and manages the everything -- it is the actual repo. Its
parent directory is the working directory.  The two do not need to be in the
same directory.  All git cares about is that it has a working directory for you
to work in and a repository directory for it to keep track of your work in.  You
can specify the repository and the working directory respectively with the
commandline arguments `--git-dir=""` and `--work-tree=""`, but by default, the
`--git-dir` is `./.git` and the `--work-tree` is `./`.

I am managing my dotfiles by making the working directory home.  This also means
that the output from git-status can be pretty nasty, full of files that I would
never want to manage, such as whatever is in `~/.cache`.  This cluttered output
simply has to be tolerated, but you can deal with it by making `~/.gitignore`.

For me, this seemingly messier approach is easier to deal with.  I am also far
less likely to deal with accidentally deleting files in the repo or replacing
them by accident with `stow`.

## How to clone this repo

On a fresh install, delete _\*everything\*_ in your home directory:

```sh
rm -r ~/
```

Create a temporary file to clone the repo into and a file called `~/.local/src`:

```sh
mkdir -p ~/tmp_file ~/.local/src
```

Then clone the directory into the temporary file:

```sh
git clone https://gitlab.com/Matthew-Tate-Scarbrough/mts-rice.git ~/tmp_file
```

Then move the repo directory from the temporary file to `~/.local/src/dotfiles`
and delete the temporary file:

```sh
mv ~/tmp_file/.git ~/.local/src/dotfiles
rm -r ~/tmp_file
```

Lastly choose a branch and perform a `git-pull`:

```sh
BRANCH="FreeBSD-hyperv" # Change to desired branch
git --git-dir="$HOME/.local/src/dotfiles" --work-tree="$HOME" switch $BRANCH
git --git-dir="$HOME/.local/src/dotfiles" --work-tree="$HOME" pull
```

## How to use this repo

On a fresh install, run

```sh
~/.local/bin/initial_setup.sh
```

In any case, to actually start using the command without loging out and in
again, source `~/.profile` (a symlink to `~/.config/sh/profile`) or
`~/.config/sh/rc.sh`.  One will start an X server, the other will not.
